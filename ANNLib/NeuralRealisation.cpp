﻿#define ANNDLL_EXPORTS
#include "NeuralRealisation.h"
using namespace std;

NeuralRealisation::NeuralRealisation(vector<size_t> conf, ANeuralNetwork::ActivationType aType, float scale)
{
	this->configuration = conf;
	this->activation_type = aType;
	this->scale = scale;

	if (conf.empty())
		return;
}

NeuralRealisation::~NeuralRealisation()
{
}

/*
 Получить строку с типом сети.
 @return описание сети, содержит запись о типе нейронной сети и авторе библиотеки.
*/
string NeuralRealisation::GetType()
{
	return ("This is Sedova's XOR 05174");
}

/**
* Спрогнозировать выход по заданному входу.
* @param input - вход, длина должна соответствовать количеству нейронов во входном слое.
* @return выход сети, длина соответствует количеству нейронов в выходном слое.
*/
vector<float> NeuralRealisation::Predict(vector<float> & input)
{
	if (!is_trained || configuration.empty() || configuration[0] != input.size())
	{
		cout << "Error" << endl;
	}

	vector<float> prev_out = input;
	vector<float> cur_out;

	for (size_t layer_idx = 0; layer_idx < configuration.size() - 1; layer_idx++)
	{
		cur_out.resize(configuration[layer_idx + 1], 0);

		for (size_t to_idx = 0; to_idx < configuration[layer_idx + 1]; to_idx++)
		{
			for (size_t from_idx = 0; from_idx < configuration[layer_idx]; from_idx++)
			{
				cur_out[to_idx] += weights[layer_idx][from_idx][to_idx] * prev_out[from_idx];
			}
			cur_out[to_idx] = Activation(cur_out[to_idx]);
		}
		prev_out = cur_out;
	}
	
	return prev_out;
}

/**
* Создать нейронную сеть
* @param configuration - конфигурация нейронной сети.
*   Каждый элемент представляет собой количество нейронов в очередном слое.
* @param activation_type - тип активационной функции (униполярная, биполярная).
* @param scale - масштаб активационной функции.
*/
ANNDLL_API shared_ptr<ANN::ANeuralNetwork> ANN::CreateNeuralNetwork(
	vector<size_t> & config,
	ANN::ANeuralNetwork::ActivationType actType,
	float scale)
{
	return make_shared<NeuralRealisation>(config, actType, scale);
}

/**
* Обучить сеть методом обратного распространения ошибки.
* В ходе работы метода, после выполнения обучения флаг is_trained должен устанавливаться в true.
* @param ann - нейронная сеть, которую необходимо обучить.
* @param inputs - входы для обучения.
* @param outputs - выходы для обучения.
* @param max_iters - максимальное количество итераций при обучении.
* @param eps - средняя ошибка по всем примерам при которой происходит остановка обучения.
* @param speed - скорость обучения.
* @param std_dump - сбрасывать ли информацию о процессе обучения в стандартный поток вывода?
*/
ANNDLL_API float ANN::BackPropTraining(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	std::vector<std::vector<float>> & inputs,
	std::vector<std::vector<float>> & outputs,
	int maxIters,
	float eps,
	float speed,
	bool std_dump
	)
{
	ann->RandomInit();
	if (inputs.size() != outputs.size())
		throw invalid_argument("invalid_argument");

	float currentError(0);
	int currentIter(0);

	do
	{
		currentError = 0;
		for (size_t countIdx = 0; countIdx < inputs.size(); countIdx++)
			currentError += BackPropTrainingIteration(ann, inputs[countIdx], outputs[countIdx], speed);

		currentIter++;
		currentError = sqrt(currentError);


		if (std_dump && currentIter % 1000 == 0)
		{
			cout << "Iteration: " << currentIter << "\tError: " << currentError << endl;
		}

		if (currentError < eps)
			ann->is_trained = true;

	} while (currentError > eps && currentIter <= maxIters);
	cout << "LastIteration: " << currentIter << "\tLastError: " << currentError << endl;
	return currentError;
}

/**
* Провести одну итерацию обучения методом обратного распространения ошибки.
* @param ann - нейронная сеть, которую необходимо обучить.
* @param input - вход для обучения.
* @param outputs - выход для обучения.
* @param speed - скорость обучения.
*/
ANNDLL_API float ANN::BackPropTrainingIteration(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	const std::vector<float>& input,
	const std::vector<float>& output,
	float speed
	)
{
	float currentError(0);

	vector<vector<float>> out(ann->configuration.size());
	//первый выход равен входу
	out[0] = input;

	//прямой ход
	for (size_t layerIdx = 0; layerIdx < ann->configuration.size() - 1; layerIdx++)
	{
		out[layerIdx + 1].resize(ann->configuration[layerIdx + 1]);
		for (size_t toIdx = 0; toIdx < ann->configuration[layerIdx + 1]; toIdx++)
		{
			out[layerIdx + 1][toIdx] = 0;
			for (size_t fromIdx = 0; fromIdx < ann->configuration[layerIdx]; fromIdx++)
			{
				out[layerIdx + 1][toIdx] += out[layerIdx][fromIdx] * ann->weights[layerIdx][fromIdx][toIdx];
			}
			out[layerIdx + 1][toIdx] = ann->Activation(out[layerIdx + 1][toIdx]);
		}
	}

	vector<vector<float>> sigma(ann->configuration.size());
	vector<vector<vector<float>>> dw(ann->configuration.size() - 1);
	sigma.back().resize(out.back().size());

	for (size_t layerIdx = 0; layerIdx < out.back().size(); layerIdx++)
	{
		sigma.back()[layerIdx] = (output[layerIdx] - out.back()[layerIdx]) *
			ann->ActivationDerivative(out.back()[layerIdx]);
		currentError += (output[layerIdx] - out.back()[layerIdx]) * (output[layerIdx] - out.back()[layerIdx]);
	}

	//обратный ход
	size_t layerIdx = ann->configuration.size() - 2;
	for (int i = ann->configuration.size() - 2; i >= 0; i--)
	{
		dw[layerIdx].resize(ann->weights[layerIdx].size());
		sigma[layerIdx].resize(ann->configuration[layerIdx], 0);

		for (size_t fromIdx = 0; fromIdx < ann->configuration[layerIdx]; fromIdx++)
		{
			for (size_t toIdx = 0; toIdx < ann->configuration[layerIdx + 1]; toIdx++)
			{
				sigma[layerIdx][fromIdx] += sigma[layerIdx + 1][toIdx] * ann->weights[layerIdx][fromIdx][toIdx];
			}

			sigma[layerIdx][fromIdx] *= ann->ActivationDerivative(out[layerIdx][fromIdx]);
			dw[layerIdx][fromIdx].resize(ann->weights[layerIdx][fromIdx].size());

			for (size_t toIdx = 0; toIdx < ann->configuration[layerIdx + 1]; toIdx++)
			{
				dw[layerIdx][fromIdx][toIdx] = speed * sigma[layerIdx + 1][toIdx] * out[layerIdx][fromIdx];
			}
		}
		layerIdx--;
	}

	//модификация весов
	for (size_t layerIdx = 0; layerIdx < ann->weights.size(); layerIdx++)
	{
		for (size_t fromIdx = 0; fromIdx < ann->weights[layerIdx].size(); fromIdx++)
		{
			for (size_t toIdx = 0; toIdx < ann->weights[layerIdx][fromIdx].size(); toIdx++)
			{
				ann->weights[layerIdx][fromIdx][toIdx] += dw[layerIdx][fromIdx][toIdx];
			}
		}
	}

	return currentError;
}