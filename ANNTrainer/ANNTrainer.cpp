#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	/*cout << "hello ANN!" << endl;*/
	cout << GetTestString().c_str() << endl; //���������, ������������ �� ����������
	vector<size_t> config; //������� ������������ ���������
	config.resize(4); //��� �� �����
	config = { 2, 5, 5, 1 }; //��� �� �����  � ������ ����
	auto pAnn = CreateNeuralNetwork(config); //������������� ���������
	vector<vector<float>> input, output; //����� � ������ (��������)
	bool success = LoadData("xor.data", input, output); //��������� �� �� �����
	BackPropTraining(pAnn, input, output, 20000, 0.1, 0.1, true);//����������� ���� ��������
	pAnn->Save("skynet.ann");//��������� ���������
	system("pause");
	return 0;
}