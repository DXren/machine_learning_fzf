#include <iostream>
#include <FeatureExtraction.h>
#include "Visualisation.h"

using namespace std;
using namespace fe;

int main() 
{
	cout << "Polinom LVL" << endl;
	cout << GetTestString().c_str() << endl;
	int lvl;
	cin >> lvl;
	
	//1
	shared_ptr<PolynomialManager> polim = CreatePolynomialManager();
	polim->InitBasis(lvl, 30);
	//2
	OrthoBasis bas = polim->GetBasis();
	ShowPolynomials("DD", bas);

	system("pause");
	return 0;
}