#include <iostream>
#include <ANN.h>
#include <conio.h>
using namespace std;
using namespace ANN;

int main()
{
	/*cout << "hello ANN!" << endl;*/
	cout << GetTestString().c_str() << endl;
	vector<vector<float>> input, output;
	shared_ptr<ANN::ANeuralNetwork> pAnn = CreateNeuralNetwork();
	cout << pAnn->GetType() << endl;
	pAnn->Load("skynet.ann");
	LoadData("xor.data", input, output);
	cout << "X\t" << "Y\t" << "Out\t" << endl;
	cout.precision(5);
	for (int i = 0; i < input.size(); i++)//проверяем правильность работы
	{
		output[i] = pAnn->Predict(input[i]);
		for (int j = 0; j < input[i].size(); j++)
			cout << input[i][j] << "\t";
		
		for (int j = 0; j < output[i].size(); j++)
			cout << output[i][j] << "\t";
		cout << endl;
	}

	return 0;
}